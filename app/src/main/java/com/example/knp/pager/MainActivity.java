package com.example.knp.pager;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<View> pages = new ArrayList<>();

        LayoutInflater inflater = LayoutInflater.from(this);
        View page = inflater.inflate(R.layout.activity_main,null);
        //TODO что то накидать
        RatingBar rb= (RatingBar) page.findViewById(R.id.ratingBar);
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(MainActivity.this, Float.toString(rating), Toast.LENGTH_SHORT).show();
            }
        });

        pages.add(page);
        page = inflater.inflate(R.layout.l2,null);
        Button b = (Button) page.findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "lol", Toast.LENGTH_SHORT).show();
            }
        });
        pages.add(page);

        ViewPager pager = new ViewPager(this);
        MyPagerAdapter adapter = new MyPagerAdapter(pages);
        pager.setAdapter(adapter);
        pager.setCurrentItem(0);
        setContentView(pager);
    }
}
